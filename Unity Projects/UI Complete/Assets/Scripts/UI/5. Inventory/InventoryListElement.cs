﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("Zadania z UI/Zadanie 5./Inventory List")]
public class InventoryListElement : MonoBehaviour {
	public Text NameLabel;
	public Text CountLabel;

	public void Setup(string name, string count){
		NameLabel.text = name;
		CountLabel.text = count;
	}
}
