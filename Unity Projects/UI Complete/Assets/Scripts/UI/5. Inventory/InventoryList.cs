﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Zadania z UI/Zadanie 5./Inventory List")]
public class InventoryList : MonoBehaviour {
	public InventoryListElement ElementPrototype;


	// Use this for initialization
	void Start () {
		int entries = Random.Range(10, 20);
		for(int i = 0; i < entries; i++)
		{
			var element = Instantiate<InventoryListElement>(ElementPrototype, 
				ElementPrototype.transform.position, ElementPrototype.transform.rotation, ElementPrototype.transform.parent);
			element.Setup(string.Format("ID{0}", Random.Range(5*entries, 10*entries)), Random.Range(3, 25).ToString());
		}

		ElementPrototype.gameObject.SetActive(false);
	}
}
