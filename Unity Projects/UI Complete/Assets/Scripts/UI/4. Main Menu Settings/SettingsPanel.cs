﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("Zadania z UI/Zadanie 4./Settings Panel")]
public class SettingsPanel : MonoBehaviour {
	public Slider MusicSlider;
	public Slider SoundSlider;
	public Toggle EasyDifficultyToggle;
	public Toggle MediumDifficultyToggle;
	public Toggle HardDifficultyToggle;
	public Toggle GoreToggle;
	public Toggle AimAssistToggle;

	public void Save(){
		Settings.SaveSettings();
	}

	void OnEnable() {
		MusicSlider.value = Settings.MusicVolume;
		SoundSlider.value = Settings.SoundVolume;

		ToggleGroup DifficultyToggleGroup = EasyDifficultyToggle.group;

		switch(Settings.Difficulty)
		{
			case DifficultyLevel.Easy:
				EasyDifficultyToggle.isOn = true;
				DifficultyToggleGroup.NotifyToggleOn(EasyDifficultyToggle);
				break;
			case DifficultyLevel.Medium:
				MediumDifficultyToggle.isOn = true;
				DifficultyToggleGroup.NotifyToggleOn(MediumDifficultyToggle);
				break;
			case DifficultyLevel.Hard:
				HardDifficultyToggle.isOn = true;
				DifficultyToggleGroup.NotifyToggleOn(HardDifficultyToggle);
				break;
		}

		GoreToggle.isOn = Settings.Gore;
		AimAssistToggle.isOn = Settings.AimAssist;

		{//Bind to events
			MusicSlider.onValueChanged.AddListener((float volume) => {Settings.MusicVolume = volume;});
			SoundSlider.onValueChanged.AddListener((float volume) => {Settings.SoundVolume = volume;});

			EasyDifficultyToggle.onValueChanged.AddListener((bool isOn) => {
					if(isOn)
					{
							Settings.Difficulty = DifficultyLevel.Easy;
					}
				});

			MediumDifficultyToggle.onValueChanged.AddListener((bool isOn) => {
					if(isOn)
					{
							Settings.Difficulty = DifficultyLevel.Medium;
					}
				});

			HardDifficultyToggle.onValueChanged.AddListener((bool isOn) => {
					if(isOn)
					{
							Settings.Difficulty = DifficultyLevel.Hard;
					}
				});

			GoreToggle.onValueChanged.AddListener((bool isOn) => {Settings.Gore = isOn;});
			AimAssistToggle.onValueChanged.AddListener((bool isOn) => {Settings.AimAssist = isOn;});
		}
	}

	void OnDisable() {
		{//Unbind from events
			MusicSlider.onValueChanged.RemoveAllListeners();
			SoundSlider.onValueChanged.RemoveAllListeners();
			EasyDifficultyToggle.onValueChanged.RemoveAllListeners();
			MediumDifficultyToggle.onValueChanged.RemoveAllListeners();
			HardDifficultyToggle.onValueChanged.RemoveAllListeners();
			GoreToggle.onValueChanged.RemoveAllListeners();
			AimAssistToggle.onValueChanged.RemoveAllListeners();
		}
	}
}
