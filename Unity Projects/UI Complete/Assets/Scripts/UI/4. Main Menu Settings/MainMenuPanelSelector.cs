﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Zadania z UI/Zadanie 4./Main Menu Selector")]
public class MainMenuPanelSelector : MonoBehaviour {
	public GameObject MainMenu;
	public GameObject Settings;

	public void ShowMainMenu(){
		Settings.gameObject.SetActive(false);
		MainMenu.gameObject.SetActive(true);
	}

	public void ShowSettings(){
		MainMenu.gameObject.SetActive(false);
		Settings.gameObject.SetActive(true);
	}

	void OnEnable() {
		ShowMainMenu();
	}
}
