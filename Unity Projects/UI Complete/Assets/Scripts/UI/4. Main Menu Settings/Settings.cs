﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class Settings {
	const string MusicVolumeKey = "Music";
	const string SoundVolumeKey = "Sound";
	const string DifficultyKey = "Difficulty";
	const string GoreKey = "Gore";
	const string AimAssistKey = "AimAssist";

	static public float MusicVolume = 0.75f;
	static public float SoundVolume = 0.75f;
	static public DifficultyLevel Difficulty = DifficultyLevel.Medium;
	static public bool Gore = true;
	static public bool AimAssist = true;

	public static Action OnSettingsLoaded = delegate{};

	[RuntimeInitializeOnLoadMethod]
	static public void LoadSettings(){
		MusicVolume = PlayerPrefs.GetFloat(MusicVolumeKey, MusicVolume);
		SoundVolume = PlayerPrefs.GetFloat(SoundVolumeKey, SoundVolume);
		Difficulty = (DifficultyLevel)PlayerPrefs.GetInt(DifficultyKey, (int)Difficulty);
		Gore = IntToBool(PlayerPrefs.GetInt(GoreKey, BoolToInt(Gore)));
		AimAssist = IntToBool(PlayerPrefs.GetInt(AimAssistKey, BoolToInt(AimAssist)));

		Debug.Log("Settings loaded");
		OnSettingsLoaded.Invoke();
	}

	static public void SaveSettings(){
		PlayerPrefs.SetFloat(MusicVolumeKey, MusicVolume);
		PlayerPrefs.SetFloat(SoundVolumeKey, SoundVolume);
		PlayerPrefs.SetInt(DifficultyKey, (int)Difficulty);
		PlayerPrefs.SetInt(GoreKey, BoolToInt(Gore));
		PlayerPrefs.SetInt(AimAssistKey, BoolToInt(AimAssist));
	}

	static bool IntToBool(int i){
		return (i == 0)?false:true;
	}

	static int BoolToInt(bool b){
		return b?1:0;
	}
}

public enum DifficultyLevel{
	Easy = 0,
	Medium,
	Hard
}
