﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("Zadania z UI/Zadanie 3./Login Screen")]
public class LoginScreen : MonoBehaviour {
	public InputField EmailField;
	public InputField PasswordField;
	public Text EmailErrorLabel;
	public Text PasswordErrorLabel;
	public Button SubmitButton;

	bool isEmailFieldValid = false;
	bool isPasswordFieldValid = false;

	Dictionary<string, string> LoginsAndPasswords = new Dictionary<string, string>()
	{
		{"asdf@giga.com", "prosiaczek123"},
		{"testowy321@baca.pl", "haslo111"},
		{"niepoprawny...mail@asd.cn", "kozak!"}
	};

	void OnEnable() {
		//Clear UI State
		EmailErrorLabel.gameObject.SetActive(false);
		PasswordErrorLabel.gameObject.SetActive(false);

		EmailField.text = "";
		PasswordField.text = "";

		SubmitButton.interactable = false;

		isEmailFieldValid = false;
		isPasswordFieldValid = false;

		{//Bind to events
			EmailField.onValueChanged.AddListener(ValidateEmail);
			PasswordField.onValueChanged.AddListener(ValidatePassword);
			SubmitButton.onClick.AddListener(Submit);
		}
	}

	void OnDisable() {
		{//Unbind from events
			EmailField.onValueChanged.RemoveListener(ValidateEmail);
			PasswordField.onValueChanged.RemoveListener(ValidatePassword);
			SubmitButton.onClick.RemoveListener(Submit);
		}
	}

	void Submit(){
		string login = EmailField.text;
		string password = PasswordField.text;

		bool showEmailError = false;
		bool showPasswordError = false;

		if(LoginsAndPasswords.ContainsKey(login))
		{
			if(LoginsAndPasswords[login] == password)
			{
				Debug.LogFormat("Correctly login as {0}", login);
			}
			else
			{
				showPasswordError = true;
			}
		}
		else
		{
			showEmailError = true;
		}

		EmailErrorLabel.gameObject.SetActive(showEmailError);
		PasswordErrorLabel.gameObject.SetActive(showPasswordError);
	}

	void ValidateEmail(string text){
		const int MinCharacters = 5;
		const int MinAppersandIndex = 1;
		const int MinLastDotIndex = 3;
		isEmailFieldValid = (text.Length >= MinCharacters);
		isEmailFieldValid &= (text.IndexOf('@') >= MinAppersandIndex);
		isEmailFieldValid &= (text.LastIndexOf('.') >= MinLastDotIndex);
		if(isEmailFieldValid)
		{
			isEmailFieldValid &= (text.IndexOf('@') < text.LastIndexOf('.'));
		}
		UpdateSubmitButtonState();
	}

	void ValidatePassword(string text){
		const int MinCharacters = 6;
		isPasswordFieldValid = (text.Length >= MinCharacters);
		UpdateSubmitButtonState();
	}

	void UpdateSubmitButtonState(){
		bool isAvaible = (isEmailFieldValid && isPasswordFieldValid);
		SubmitButton.interactable = isAvaible;
	}
}
